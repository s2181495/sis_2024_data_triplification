import sys
import pandas as pd
from rdflib import *

def transform_dataset(dataset_path):
    # Load dataset
    data_frame = pd.read_csv(dataset_path)
    data_frame = data_frame.drop(['cet_cest_timestamp', 'interpolated'], axis=1)

    # Define RDF namespace
    SAREF_NS = Namespace('https://saref.etsi.org/core/v3.1.1/saref.ttl')

    # Create RDF graph and bind prefixes
    rdf_graph = Graph()
    rdf_graph.bind('saref', SAREF_NS)

    # Initialize measurement ID
    measurement_id = 0

    # Iterate over columns in the dataset
    for col_name in data_frame.columns:
        # Create a URI for each sensor
        sensor_uri = URIRef("http://example.org/sensor/" + col_name)
        # Iterate over each row in the dataset
        for idx, row in data_frame.iterrows():
            # Create a URI for each measurement
            measure_uri = URIRef("http://example.org/measure/" + str(measurement_id))
            if col_name != "utc_time":
                # Add triples to the RDF graph
                rdf_graph.add((measure_uri, RDF.type, SAREF_NS.Measurement))
                rdf_graph.add((measure_uri, SAREF_NS.hasTimestamp, Literal(row["utc_time"])))
                rdf_graph.add((measure_uri, SAREF_NS.hasValue, Literal(row[col_name])))
                rdf_graph.add((sensor_uri, RDF.type, SAREF_NS.Sensor))
                rdf_graph.add((sensor_uri, SAREF_NS.producesMeasurement, measure_uri))
                measurement_id += 1

    # Serialize and save the RDF graph
    rdf_graph.serialize(format='turtle', destination="rdf_data.ttl")

def main():
    dataset_path = sys.argv[1]
    transform_dataset(dataset_path)

if __name__ == "__main__":
    main()
